#!/usr/bin/env python3

import tensorflow as tf
import numpy as np
import retro

from skimage import transform
from skimage.color import rgb2gray

import matplotlib.pyplot as plt

from collections import deque

import random 
import warnings

warnings.filterwarnings('ignore')

# Create our environment
env = retro.make(game='SpaceInvaders-Atari2600')

print("The size of our frame is: ", env.observation_space)
print("The action size is: ", env.action_space.n)

# Create one hot encoded version of our actions
possible_actions = np.array( np.identity(env.action_space.n, dtype=int).tolist() )

def preprocess_frame(frame):
    gray = rgb2gray(frame)
    # Crop the screen
    # [Up: Down, Left: Right]
    cropped_frame = gray[8:-12,4:-12]

    normalized_frame = cropped_frame/255.0

    preprocessed_frame = transform.resize(cropped_frame, [110,84])

    return preprocessed_frame

stack_size = 4

stacked_frames = deque([np.zeros((110,84), dtype=np.int) for i in range(stack_size)], maxlen=4)

def stack_frames(stacked_frames, state, is_new_episode):
    frame = preprocess_frame(state)

    if is_new_episode:
        stacked_frames = deque([np.zeros((110,84), dtype=np.int) for i in range(stack_size)], maxlen=4)

        stacked_frames.append(frame)
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        stacked_frames.append(frame)
        
        stacked_state = np.stack(stacked_frames, axis=2)

    else:
        stacked_frames.append(frame)

        stacked_state = np.stack(stacked_frames, axis=2)

    return stacked_state, stacked_frames


state_size = [110, 84, 4]
action_size = env.action_space.n
learning_rate = 0.00025

total_episodes = 200
max_steps = 50000
batch_size = 64

explore_start = 1.0
explore_stop = 0.01
decay_rate = 0.00001

gamma = 0.9

pretrain_length = 1000

memory_size = 1000

stack_size = 4

training = True

episode_render = False

class DQNetwork:
    def __init__(self, state_size, action_size, learning_rate, name='DQNetwork'):
        self.state_size = state_size
        self.action_size = action_size
        self.learning_rate = learning_rate

        with tf.variable_scope(name):
            # We create the placeholders
            # *state_size means that we take each elements of state_size in tuple hence is like if we wrote
            # [None, 84,84,4]
            self.inputs_ = tf.placeholder(tf.float32, [None, *state_size], name="inputs")
            self.actions_ = tf.placeholder(tf.float32, [None, self.action_size], name="actions_")

            # Remember that target_Q is the R(s,a) + ymax Qhat(s', a')
            self.target_Q = tf.placeholder(tf.float32, [None], name="target")

            """
            First convnet:
            CNN
            ELU
            """

            # Input is 110x84x4
            self.conv1 = tf.layers.conv2d(inputs = self.inputs_,
                    filters = 32,
                    kernel_size = [8,8],
                    strides = [4,4],
                    padding = "VALID",
                    kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d(),
                    name = "conv1")
            self.conv1_out = tf.nn.elu(self.conv1, name="conv1_out")

            """
            Sconed convnet:
            CNN
            ELU
            """

            self.conv2 = tf.layers.conv2d(inputs = self.conv1_out,
                    filters = 64,
                    kernel_size = [4,4],
                    strides = [2,2],
                    padding = "VALID",
                    kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d(),
                    name = "conv2")
            self.conv2_out = tf.nn.elu(self.conv2, name="conv2_out")

            """
            Third convnet:
            CNN
            ELU
            """
            self.conv3 = tf.layers.conv2d(inputs = self.conv2_out,
                    filters = 64, 
                    kernel_size = [3,3],
                    strides = [2,2],
                    padding = "VALID",
                    kernel_initializer = tf.contrib.layers.xavier_initializer_conv2d(),
                    name = "conv3")
            self.conv3_out = tf.nn.elu(self.conv3, name="conv3_out")

            self.flatten = tf.contrib.layers.flatten(self.conv3_out)
            self.fc = tf.layers.dense(inputs = self.flatten,
                    units = 512,
                    activation = tf.nn.elu,
                    kernel_initializer=tf.contrib.layers.xavier_initializer(),
                    name="fc1")
            self.output = tf.layers.dense(inputs = self.fc,
                    kernel_initializer=tf.contrib.layers.xavier_initializer(),
                    units = self.action_size,
                    activation=None)

            # Q is our predicted Q value
            self.Q = tf.reduce_sum(tf.multiply(self.output, self.actions_))

            # The loss is the difference between our predicted Q_values and the Q_target
            # Sum(Qtarget - Q)^2
            self.loss = tf.reduce_mean(tf.square(self.target_Q - self.Q))

            self.optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)


# Reset the graph
tf.reset_default_graph()

# Instantiate the DQNetwork
DQNetwork = DQNetwork(state_size, action_size, learning_rate)

class Memory():
    def __init__(self, max_size):
        self.buffer = deque(maxlen = max_size)
    
    def add(self, experience):
        self.buffer.append(experience)

    def sample(self, batch_size):
        buffer_size = len(self.buffer)
        index = np.random.choice(np.arange(buffer_size),
                size = batch_size,
                replace = False)
        return [self.buffer[i] for i in index]
    
# Instantiate memory
memory = Memory(max_size = memory_size)
for i in range(pretrain_length):
    # If it's the first step
    if i == 0:
        state = env.reset()

        state, stacked_frames = stack_frames(stacked_frames, state, True)
    choice = random.randint(1, len(possible_actions))-1
    action = possible_actions[choice]
    next_state, reward, done, _ = env.step(action)

    # env.render()

    # Stack the frames
    next_state, stacked_frames = stack_frames(stacked_frames, next_state, False)
    if done:
        # We finished the episode
        next_state = np.zeros(state.shape)
        
        # Add experience to memory
        memory.add((state, action, reward, next_state, done))

        # Start a new episode
        state = env.reset()

        # Stack the frames
        state, stacked_frames = stack_frames(stacked_frames, state, True)
    else:
        # add experience to memory
        memory.add((state, action, reward, next_state, done))

        state = next_state

# Setup TensorBoard Writer
writer = tf.summary.FileWriter("./tensorboard/DQN1_v2")

## Losses
tf.summary.scalar("Loss", DQNetwork.loss)

write_op = tf.summary.merge_all()

def predict_action(explore_start, explore_stop, decay_rate, decay_step, state, actions):
    exp_exp_tradeoff = np.random.rand()

    explore_probability = explore_stop + (explore_start - explore_stop) * np.exp(-decay_rate * decay_step)
    if (explore_probability > exp_exp_tradeoff):
        # Make a random action (exploration)
        choice = random.randint(1, len(possible_actions))-1
        action = possible_actions[choice]

    else:
        # Get action from Q-network (exploitation)
        # Estimate the Qs values state
        Qs = sess.run(DQNetwork.output, feed_dict = {DQNetwork.inputs_: state.reshape((1, *state.shape))})

        # Take the biggest Q value (= the best action)
        choice = np.argmax(Qs)
        action = possible_actions[choice]

    return action, explore_probability

TotalRewardList = []

# Saver will help us save our model
saver = tf.train.Saver()

if training == True:
    config = tf.ConfigProto(allow_soft_placement = True)
    config.gpu_options.allow_growth = True

    with tf.Session(config = config) as sess:
        # Initialize the variables
        sess.run(tf.global_variables_initializer())
        # Initialize the decay rate (that will use to reduce epsilon)
        decay_step = 0

        for episode in range(total_episodes):
            step = 0

            episode_rewards = []

            state = env.reset()

            state, stacked_frames = stack_frames(stacked_frames, state, True)
            while step < max_steps:
                step += 1

                decay_step += 1

                # Predict the action to take and take it
                action, explore_probability = predict_action(explore_start, explore_stop, decay_rate, decay_step, state, possible_actions)

                # Perform the action and get the next_state, reward, and done information
                next_state, reward, done, _ = env.step(action)

                if episode_render:
                    env.render()

                episode_rewards.append(reward)

                if done:
                    # The episode ends so no next state
                    next_state = np.zeros((110,84), dtype=np.int)
                    
                    next_state, stacked_frames = stack_frames(stacked_frames, next_state, False)

                    # Set step = max_steps to end the episode
                    step = max_steps

                    # Get the total reward of the episode
                    total_reward = np.sum(episode_rewards)
                    
                    TotalRewardList.append(total_reward)     

                    print('Episode: {}'.format(episode),
                      'Total reward: {}'.format(total_reward),
                      'Explore P: {:.4f}'.format(explore_probability),
                      'Training Loss {:.4f}'.format(loss))

                    # Store transition <st,at,rt+1,st+1> in memory D
                    memory.add((state, action, reward, next_state, done))

                else:
                    #print("Not done, episode ", episode, ",Step: ", step)
                    # Stack the frame of the next_state
                    next_state, stacked_frames = stack_frames(stacked_frames, next_state, False)
                
                    # Add experience to memory
                    memory.add((state, action, reward, next_state, done))

                    # st+1 is now our current state
                    state = next_state
	    
                ### LEARNING PART            
                # Obtain random mini-batch from memory
                batch = memory.sample(batch_size)
                states_mb = np.array([each[0] for each in batch], ndmin=3)
                actions_mb = np.array([each[1] for each in batch])
                rewards_mb = np.array([each[2] for each in batch]) 
                next_states_mb = np.array([each[3] for each in batch], ndmin=3)
                dones_mb = np.array([each[4] for each in batch])

                target_Qs_batch = []

                # Get Q values for next_state 
                Qs_next_state = sess.run(DQNetwork.output, feed_dict = {DQNetwork.inputs_: next_states_mb})
                
                # Set Q_target = r if the episode ends at s+1, otherwise set Q_target = r + gamma*maxQ(s', a')
                for i in range(0, len(batch)):
                    terminal = dones_mb[i]

                    # If we are in a terminal state, only equals reward
                    if terminal:
                        target_Qs_batch.append(rewards_mb[i])
                    
                    else:
                        target = rewards_mb[i] + gamma * np.max(Qs_next_state[i])
                        target_Qs_batch.append(target)
                    

                targets_mb = np.array([each for each in target_Qs_batch])

                loss, _ = sess.run([DQNetwork.loss, DQNetwork.optimizer],
                               feed_dict={DQNetwork.inputs_: states_mb,
                                        DQNetwork.target_Q: targets_mb,
                                      DQNetwork.actions_: actions_mb})

                # Write TF Summaries
                summary = sess.run(write_op, 
			    feed_dict={DQNetwork.inputs_: states_mb,
                                   DQNetwork.target_Q: targets_mb,
                                   DQNetwork.actions_: actions_mb})
                writer.add_summary(summary, episode)
                writer.flush()

            # Save model every 5 episodes
            if episode % 5 == 0:
                save_path = saver.save(sess, "./models/model_v2.ckpt")
                print("Model Saved")

print("Total Reward List: ", TotalRewardList)

"""
with tf.Session() as sess:
    total_test_rewards = []
    
    # Load the model
    saver.restore(sess, "./models/model.ckpt")
    
    for episode in range(1):
        total_rewards = 0
        
        state = env.reset()
        state, stacked_frames = stack_frames(stacked_frames, state, True)
        
        print("****************************************************")
        print("EPISODE ", episode)
        
        while True:
            # Reshape the state
            state = state.reshape((1, *state_size))
            # Get action from Q-network 
            # Estimate the Qs values state
            Qs = sess.run(DQNetwork.output, feed_dict = {DQNetwork.inputs_: state})
            
            # Take the biggest Q value (= the best action)
            choice = np.argmax(Qs)
            action = possible_actions[choice]
            
            #Perform the action and get the next_state, reward, and done information
            next_state, reward, done, _ = env.step(action)
            env.render()
            
            total_rewards += reward

            if done:
                print ("Score", total_rewards)
                total_test_rewards.append(total_rewards)
                break
                
                
            next_state, stacked_frames = stack_frames(stacked_frames, next_state, False)
            state = next_state
            
    env.close()


"""




